# Efficient Auto-Tuning of Parallel Programs with Interdependent Tuning Parameters

## Introduction
This artifact describes the experiments that were conducted for the paper *Efficient Auto-Tuning of Parallel Programs with Interdependent Tuning Parameters*. The artifact is provided as a Docker image and alternatively also as plain source code files. Detailed instructions how to execute the experiments can be found in [Experiment workflow using Docker](https://gitlab.com/anonymous_author/ipdps18_atf_artifact/blob/master/Readme.md#experiment-workflow-using-docker) and [Experiment workflow without Docker](https://gitlab.com/anonymous_author/ipdps18_atf_artifact/blob/master/Readme.md#experiment-workflow-without-docker).

We compare ATF with [CLTune](https://github.com/CNugteren/CLTune) version 2.7.0 and [OpenTuner](https://github.com/jansel/opentuner) version 0.8.0. When auto-tuning BLAS routines for deep learning, we take implementations from the open-source library [CLBlast](https://github.com/CNugteren/CLBlast) version 1.4.1, and we use the [Caffe](https://github.com/BVLC/caffe) deep-learning framework version 1.0. 

## Software dependencies
The artifact is delivered as a Docker image, thus freeing the reviewer from installing any software dependencies except for Docker itself. The Docker image contains the OpenCL runtime environment for Intel CPUs. In case a reviewer intends to evaluate our experiments on devices that are different from Intel CPU, the corresponding driver and OpenCL runtime have to be installed inside the Docker container; the Docker image is based on Ubuntu 16.04 -- additional software can be installed inside the Docker container via the APT package manager. Note that in case of targeting NVIDIA GPUs, it is necessary to install the exact same driver version inside the Docker container that is also used on the host system.

In case the reviewer intends to execute our experiments without using Docker, the following software dependencies have to be installed:
- OpenTuner 0.8.0
- an OpenCL driver and runtime environment
- Python 2.7 (not Python 3.x)
- a compiler supporting C++14 or higher
- CMake 2.8.11 or higher

## Experiment workflow using Docker
The reviewer is invited to perform the following steps:
1. Obtain Docker image:
    
    `$ wget https://gitlab.com/anonymous_author/ipdps18_atf_artifact/raw/master/atf_artifact_docker.tar`
2. Load our Docker image:
    
    `$ docker load < atf_artifact_docker.tar`
3. Run our Docker image:
    
    `$ docker run --name atf_artifact -it atf_artifact /bin/bash`
    
    In case the reviewer intends to execute our experiments on a GPU, additional flags have to be passed to our Docker image. First, a list of the necessary device files has to be obtained; for example, for an NVIDIA GPU:
    
    `$ ls /dev/nvidia*`
    
    All listed device files have to be appended to the `docker run` command using the `--device` flag as follows:
    
    ```
    $ docker run --device=/dev/nvidia0:/dev/nvidia0
        --device=/dev/nvidia-uvm:/dev/nvidia-uvm
        --device=/dev/nvidiactl:/dev/nvidiactl
        --name atf_artifact -it atf_artifact /bin/bash
    ```
4. (*Optional*) Additional drivers have to be installed in case the evaluation is not performed on an Intel CPU -- the `apt` command can be used for this.
    
    __Important:__ Note that in order to use an NVIDIA GPU, the exact same version of the NVIDIA driver used in the host system has to be installed inside our Docker container.
5. Inside our Docker container, change into the artifact directory:
    
    `$ cd /artifact`
6. Select the desired OpenCL platform and device id by edditing the file `inputs.json` in line 2 and 3. All platforms and devices can be listed with `clinfo`.
7. Perform the CLBlast tuning:
    
    `$ ./scripts/tune_clblast.sh`
8. Run our experiments:
    
    `$ ./scripts/run_all_experiments.sh`
9. Run the references:
    
    `$ ./scripts/run_all_references.sh`
10. Plot the result graphs:
    
    `$ ./scripts/plot_graphs.sh`

In case the reviewer terminates the Docker container, it can be restored by using the command `docker start -ia atf_artifact`.

__Note:__ When changing the platform or device id in the `inputs.json` file, make sure to clean previous results via `$ ./scripts/clean_results.sh` and to run the CLBlast tuning (Step 7 of the above enumeration) again.

## Experiment workflow without Docker
The reviewer is invited to perform the following steps:
1. Download the artifact archive:
    
    `$ wget https://gitlab.com/anonymous_author/ipdps18_atf_artifact/raw/master/atf_artifact.tar.gz`
2. Unpack the archive:
    
    `$ tar -xzf atf_artifact.tar.gz`

3. Change into the artifact directory:

    `$ cd artifact`

    In order to use the artifact scripts set the environment variable `ARTIFACT_DIR` to the artifact directory. Execute

    ```
    $ export ARTIFACT_DIR=`pwd`
    ```

    inside the artifact directory.
       
4. Compile our artifact source code:

    `$ ./scripts/install.sh`

    All arguments provided to the `install.sh` script will be directly passed to CMake when building the binaries. This can be used for example, if the dependencies are not installed in the default paths or can not be found by CMake.

5. Select the desired OpenCL platform and device id by edditing the file `inputs.json` in line 2 and 3. All platforms and devices can be listed with `clinfo`.
6. Perform the CLBlast tuning:
    
    `$ ./scripts/tune_clblast.sh`
7. Run our experiments:
    
    `$ ./scripts/run_all_experiments.sh`
8. Run the references:
    
    `$ ./scripts/run_all_references.sh`
9. Plot the result graphs:
    
    `$ ./scripts/plot_graphs.sh`

__Note:__ When changing the platform or device id in the `inputs.json` file, make sure to clean previous results via `$ ./scripts/clean_results.sh` and to run the CLBlast tuning (Step 6 of the above enumeration) again.
